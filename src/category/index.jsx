import React from "react";
import { Table, Form, Button, ButtonGroup } from "react-bootstrap";
import { BsPencil, BsTrash } from "react-icons/bs"
import FormCrud from "./form";

import Actions from "../enum/action";
import AlertDismissible from "../messages/alertDismissible";

class Category extends React.Component {
    item = {
        Id: 0, Initial: '', Name: '', Active: true
    }

    constructor() {
        super();
        this.state = {
            list: [
                { Id: 1, Initial: 'MainCo', Name: 'Main Course', Active: true },
                { Id: 2, Initial: 'DRK', Name: 'Drink', Active: true },
                { Id: 3, Initial: 'ICE', Name: 'Ice Cream', Active: false },
                { Id: 4, Initial: 'DSR', Name: 'Dessert', Active: true }
            ],
            item: this.item,
            showForm: false,
            showMess: false,
            action: null
        }
    }

    handleCreate = () => {
        this.setState({
            showForm: true,
            item: this.item,
            action: Actions.CREATE
        })
    }

    handleClose = () => {
        this.setState({
            showForm: false
        })
    }

    handleChangeValue = name => ({ target: { value } }) => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    handleChangeChecked = name => (event) => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: event.target.checked
            }
        })
    }

    handleSubmit = (event) => {
        const { list, item, action } = this.state;

        if (action === Actions.CREATE) {
            const lastId = Math.max.apply(Math,
                this.state.list.map(o => { return o.Id })) + 1;
            item.Id = lastId;
            list.push(item);
            this.setState({
                list: list,
                showForm: false,
                action: action,
                showMess: true
            })
        } else if (action === Actions.EDIT){
            const idx = this.state.list.findIndex(o => o.Id === item.Id)
            list[idx] = item;
            this.setState({
                list: list,
                showForm:false,
                action: action,
                showMess: true
            })
        } else {
            const idx = this.state.list.findIndex(o => o.Id === item.Id)
            list.splice(idx, 1);
            this.setState({
                list: list,
                showForm:false,
                action: action,
                showMess: true
            })
        }
    }

    handleSelect = (id, action) => {
        const item = this.state.list.find(o => o.Id === id);
        this.setState({
            item: item,
            action: action,
            showForm: true
        })
    }

    handleCloseAlert = () => {
        this.setState({
            showMess: false
        })
    }

    render() {
        const { list, showForm, showMess, item, action } = this.state;
        return (
            <>
                <h3>List of Category</h3>
                <FormCrud
                    show={showForm}
                    handleCreate={this.handleCreate}
                    handleClose={this.handleClose}
                    item={item}
                    handleChangeValue={this.handleChangeValue}
                    handleChangeChecked={this.handleChangeChecked}
                    handleSubmit={this.handleSubmit}
                    action={action}
                />
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Initial</th>
                            <th>Name</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map(i => {
                            return (
                                <tr key={i.Id}>
                                    <td>{i.Initial}</td>
                                    <td>{i.Name}</td>
                                    <td><Form.Check type="checkbox" checked={i.Active} /></td>
                                    <td>
                                        <ButtonGroup size='sm'>
                                            <Button variant='warning' onClick={() => this.handleSelect(i.Id, Actions.EDIT)}>
                                                <BsPencil size={20} />
                                            </Button>
                                            <Button variant='danger' onClick={() => this.handleSelect(i.Id, Actions.DELETE)}>
                                                <BsTrash size={20} />
                                            </Button>
                                        </ButtonGroup>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </Table>
                {
                    showMess ?
                        <AlertDismissible
                            showMess={showMess}
                            headerText={{...action}.btnLabel}
                            bodyText="Success"
                            handleCloseAlert={this.handleCloseAlert}
                        />
                        : ``
                }
            </>
        )
    }
}

export default Category;