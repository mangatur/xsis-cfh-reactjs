import React from "react";
import { Button, Modal, Form } from "react-bootstrap";

class FormComp extends React.Component {
    render() {
        const {
            show,
            handleCreate,
            handleClose,
            handleChangeValue,
            handleChangeChecked,
            handleSubmit
        } = this.props;
        const action = { ...this.props.action };
        const { Initial, Name, Active } = this.props.item;
        return (
            <>
                <Button variant="primary" onClick={handleCreate}>Create New</Button>
                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>{action.header}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form noValidate validated={true}>
                            <Form.Group>
                                <Form.Label>Initial</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    value={Initial}
                                    onChange={handleChangeValue("Initial")}
                                    placeholder="Enter Initial"
                                    disabled={action.disabled}
                                    minLength="2"
                                    maxLength="10"
                                />
                                <Form.Control.Feedback type="invalid">
                                    Min. 2 and Max. 10
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Name</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    value={Name}
                                    onChange={handleChangeValue("Name")}
                                    placeholder="Enter Name"
                                    disabled={action.disabled}
                                    minLength="3"
                                    maxLength="50"
                                />
                                <Form.Control.Feedback type="invalid">
                                    Min. 3 and Max. 10
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group>
                                <Form.Check
                                    type="checkbox"
                                    label="Active"
                                    checked={Active}
                                    onChange={handleChangeChecked("Active")}
                                    disabled={action.disabled}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Button
                                    type="button"
                                    variant="secondary"
                                    onClick={handleClose}
                                >
                                    Cancel
                                </Button>
                                <Button
                                    type="button"
                                    variant={action.variant}
                                    onClick={handleSubmit}
                                >
                                    {action.btnLabel}
                                </Button>
                            </Form.Group>
                        </Form>
                    </Modal.Body>
                </Modal>
            </>
        )
    }
}

export default FormComp;