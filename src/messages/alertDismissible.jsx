import React from "react";
import { Toast } from "react-bootstrap";

function AlertDismissible({
    headerText = 'Some header',
    subHeaderText = 'Some sub header',
    bodyText = 'Some body text',
    showMess, handleCloseAlert }) {
    if (showMess) {
        return (
            <Toast show={showMess}
                onClose={handleCloseAlert}
                delay={5000} autohide
                style={{position: 'absolute', top: 0, right: 0, minWidth: 300 }}
            >
                <Toast.Header>
                    <img
                        src="holder.js/20x20?text=%20"
                        className="rounded mr-2"
                        alt=""
                    />
                    <strong className="mr-auto">{headerText}</strong>
                    <small>{subHeaderText}</small>
                </Toast.Header>
                <Toast.Body>
                    {bodyText}
                </Toast.Body>
            </Toast>
        )
    }
}

export default AlertDismissible;