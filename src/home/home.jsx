import React from "react";
import Room from "./room";

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            interest : [
                { Id: 1, Room: 'Alpha', Subject: 'React Js' },
                { Id: 2, Room: 'Betha', Subject: 'Node Js' },
                { Id: 3, Room: 'Gamma', Subject: 'Angular' },
                { Id: 4, Room: 'Delta', Subject: 'Vue Js' },
            ]
        }
    }

    room = (name, subject) => {
        return (
            <p>Room : {name}, Subject: {subject}</p>
        )
    }

    render() {
        const { name, city } = this.props;
        return (
            <>
                <h3>Welcome, {name} from {city}</h3>
                {/* {
                    this.state.interest.map(i => {
                        return(
                            <Room name={i.Room} subject={i.Subject}/>
                        )
                    })
                } */}
            </>
        )
    }
}

export default Home;