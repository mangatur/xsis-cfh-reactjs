const Actions = {
    CREATE: {
        header: 'Create New',
        btnLabel: 'Create New',
        variant: 'success',
        disabled: ''
    },
    EDIT: {
        header: 'Edit',
        btnLabel: 'Save Change',
        variant: 'warning',
        disabled: ''
    },
    DELETE: {
        header: 'Are you sure to remove this?',
        btnLabel: 'Remove',
        variant: 'danger',
        disabled: 'disabled'
    },
}

export default Actions