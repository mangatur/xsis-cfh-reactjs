import React from 'react';
import './App.css';
import NavBarMenu from "../src/layout/navbar";
import {
  BrowserRouter as Router, Switch, Route
} from "react-router-dom";
import { Container } from "react-bootstrap";

import Home from "../src/home/home";

import Category from "../src/category";

function App() {
  return (
    <Router>
      <Container>
        <NavBarMenu />
        <Switch>
           <Route
                exact path="/"
                render={() => (
                  <Home name="Atur Aritonang" city="Bekasi" />
                )} />
           <Route exact path='/category' component={Category} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;
