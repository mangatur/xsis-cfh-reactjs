import React from "react";
import {
    Navbar, Nav, Form, FormControl, Button
} from "react-bootstrap";
import { Link } from "react-router-dom";

class NavBarMenu extends React.Component {
    render() {
        // Testing
        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand>
                    <Link to='/'>React Js Class</Link>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link>
                            <Link to='/'>Home</Link>
                        </Nav.Link>
                        <Nav.Link>
                            <Link to='/category'>Category</Link>
                        </Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default NavBarMenu;